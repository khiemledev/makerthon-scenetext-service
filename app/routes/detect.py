from fastapi import APIRouter, Form, File, UploadFile
from src.detector import ABCNetv2
from PIL import Image
import numpy as np
import io

detector = ABCNetv2('config/abcnet_v2.yaml')

router = APIRouter()

@router.post('/detect')
def detect(file: UploadFile = File(...)):
    # Read image from bytes
    image = Image.open(io.BytesIO(file.file.read()))
    # Convert image to BGR
    image = np.asarray(image)[:, :, ::-1]

    bboxes = detector.detect(image)

    return {
        'code': '1000',
        'data': {
            'text_instances': len(bboxes),
            'bboxes': bboxes
        }
    }