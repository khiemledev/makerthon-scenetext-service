from fastapi import APIRouter, Response
from pydantic import BaseModel
import requests
import io
from PIL import Image

router = APIRouter()

class DataModel(BaseModel):
    path: str

@router.post('/download_image')
def download_image(body: DataModel):
    try:
        max_width = 1824
        im = Image.open(requests.get(body.path, stream=True).raw)
        width, height = im.size
        if width > max_width:
            im.resize((max_width, int(height * max_width / width)), Image.LANCZOS)

        buffer = io.BytesIO()
        im.save(buffer, format='JPEG', quality=95)
        return Response(buffer.getvalue(), media_type='image/jpeg')
    except:
        return {
            'code': '1001',
            'status': 'URL not found or file is not an image'
        }
