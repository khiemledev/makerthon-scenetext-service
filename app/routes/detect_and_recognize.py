from fastapi import APIRouter, UploadFile, File
from PIL import Image
import numpy as np
import io

from routes.detect import detector
from routes.recognize import recognizer

router = APIRouter()

@router.post('/detect_and_recognize')
def detect_and_recognize(file: UploadFile = File(...)):
    # Read image from bytes
    image = Image.open(io.BytesIO(file.file.read()))
    # Convert image to BGR
    image = np.asarray(image)[:, :, ::-1]

    bboxes = detector.detect(image)

    texts, scores = recognizer.crop_and_recognize(image, bboxes, return_prob=True)

    return {
        'code': '1000',
        'data': {
            'text_instances': len(bboxes),
            'texts': [(bbox, text, score) for bbox, text, score in zip(bboxes, texts, scores)]
        }
    }
