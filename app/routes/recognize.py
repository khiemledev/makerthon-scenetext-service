from fastapi import APIRouter, Form, File, UploadFile
from src.recognizer import VietOCR
from PIL import Image
import numpy as np
import io

recognizer = VietOCR('config/vietocr.yaml')

router = APIRouter()

@router.post('/recognize')
def detect(file: UploadFile = File(...)):
    # Read image from bytes
    image = Image.open(io.BytesIO(file.file.read()))
    # Convert image to BGR
    image = np.asarray(image)[:, :, ::-1]

    text, score = recognizer.recognize(image, return_prob=True)

    return {
        'code': '1000',
        'data': {
            'score': score,
            'text': text
        }
    }